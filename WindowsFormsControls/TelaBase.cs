﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsControls
{
    //Classe abstrata: não permite criar objetos.
    public abstract partial class TelaBase : Form
    {
        public TelaBase()
        {
            InitializeComponent();

        }

        private void TelaBase_Load(object sender, EventArgs e)
        {

        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            TelaDeCadastro telaCadastro = new TelaDeCadastro(this);
            telaCadastro.Show();            
        }

        private void calculadoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Calculadora calculadora = new Calculadora();
            //calculadora.Show();

            new Calculadora().Show();
        }


      
      
    }
}
