﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsControls
{
    public partial class TelaDeCadastro : TelaBase
    {
        private TelaBase telaAnterior;

        public TelaDeCadastro()
        {
            InitializeComponent();
        }

        //Sobrecarga de métodos.
        public TelaDeCadastro(TelaBase telaAnterior)
        {
            InitializeComponent();
            this.telaAnterior = telaAnterior;
        }

        //Sobrescrita de métodos.
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            this.telaAnterior.Show();
        }

    }
}
